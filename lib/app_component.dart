import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import 'src/hero_service.dart';
import 'src/hero_list_component.dart';
import 'src/routes.dart';
import 'src/messages_component.dart';
import 'src/message_service.dart';

@Component(
  selector: 'my-app',
  template: '''
    <h1>{{title}}</h1>
    <nav>
      <a [routerLink]="routes.heroes.toUrl()"
         routerLinkActive="active">Heroes</a>
      <a [routerLink]="routes.dashboard.toUrl()"
         routerLinkActive="active">Dashboard</a>
    </nav>
    <router-outlet [routes]="routes.all"></router-outlet>
    <message></message>
  ''',
  styleUrls: ['app_component.css'],
  directives: [HeroListComponent, routerDirectives, MessageComponent],
  providers: [
    ClassProvider(HeroService),
    ClassProvider(Routes),
    ClassProvider(MessageService),
  ],
)
class AppComponent {
  final title = 'Tour of Heroes - Route links';
  final Routes routes;
  AppComponent(this.routes);
}
