import 'package:angular/angular.dart';
import 'message_service.dart';
@Component(
  selector: 'message',
  templateUrl: 'message_component.html',
  styleUrls: ['message_component.css'],
  directives: [coreDirectives],
)
class MessageComponent implements OnInit{
  final MessageService messageService;
  MessageComponent(this.messageService);

  void ngOnInit() {
  }
}