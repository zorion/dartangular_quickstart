import 'package:angular_router/angular_router.dart';

final heroes = RoutePath(path: 'heroes');
final dashboard = RoutePath(path: 'dashboard');
final idParam = 'id';
final hero = RoutePath(path: '${heroes.path}/:${idParam}');

int getId(Map<String, String> parameters) {
  final id = parameters['id'];
  return id == null ? null : int.tryParse(id);
}
