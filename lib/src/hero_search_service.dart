import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';

import 'hero.dart';
import 'message_service.dart';

class HeroSearchService {
  final Client _http;
  final MessageService _messageService;

  HeroSearchService(this._http, this._messageService);

  Future<List<Hero>> search(String term) async {
    try {
      final response = await _http.get('app/heroes/?name=$term');
      List<Hero> resultList = (_extractData(response) as List)
          .map((json) => Hero.fromJson(json))
          .toList();
      _messageService.add('Got heroes for $term');
      return resultList;
    } catch (e) {
      throw _handleError(e);
    }
  }

  dynamic _extractData(Response resp) => json.decode(resp.body)['data'];

  Exception _handleError(dynamic e) {
    print(e); // for demo purposes only
    return Exception('Server error; cause: $e');
  }
}
