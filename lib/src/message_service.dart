

class MessageService {
  List<String> messages = [];
  void add(String message) {
    var now = new DateTime.now();
    messages.add(message + "  (" + now.toIso8601String() + ")");
  }
  void clear() {
    messages = [];
  }
}